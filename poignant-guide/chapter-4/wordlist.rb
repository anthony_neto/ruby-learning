#!/opt/ruby/bin/ruby

# In order to use this hash inside of the require section, needs to be a class variable.
@code_words = {
  'starmonkeys' => 'Phile and Pete, those prickly chancellors of the New Reich',
  'catapult' => 'chucky go-go',
  'firebomb' => 'Heat-Assisted Living',
  'Nigeria' => "Ny and Jerry's Dry Cleaning (with Donuts)",
  'Put the kabosh on' => 'Put the cable box on'
}
