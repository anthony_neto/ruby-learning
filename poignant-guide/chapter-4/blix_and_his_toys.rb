#!/opt/ruby/bin/ruby

kitty_toys =
  [:shape => 'sock', :fabric => 'cashmere'] +
  [:shape => 'mouse', :fabric => 'calico'] +
  [:shape => 'eggroll', :fabric => 'chenille']

# One Liner...
puts kitty_toys.sort_by { |toy| toy[:fabric] }

# NEWLINE the ORIGINAL ... and my favorite
puts

# Example
kitty_toys.sort_by { |toy| toy[:shape] }.each do |toy|
  puts "Blixy has a #{ toy[:shape] } made of #{ toy [:fabric] }"
end

# NEWLINE the sequel
puts

# Next Example
puts 'next, a method to skip' 
non_eggroll = 0
kitty_toys.each do |toy|
  next if toy[:shape] == 'eggroll'
  non_eggroll = non_eggroll + 1
  puts toy
end

# Break Example
puts

puts 'break, a method to eject out of the loop... pull your shute e9, e9, e9 e9 e9 e9.'
kitty_toys.each do |toy|
  break if toy[:fabric] == 'chenille'
  p toy
end
