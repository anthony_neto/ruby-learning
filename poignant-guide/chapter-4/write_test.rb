#!/opt/ruby/bin/ruby

print 'filename: '
filename = gets.chomp
print 'input: '
input = gets

Dir::mkdir( './write-test-output' ) unless Dir.exists?( './write-test-output' )

File::open( "./write-test-output/#{filename}.txt", 'w' ) do |f|
  f << input
end
