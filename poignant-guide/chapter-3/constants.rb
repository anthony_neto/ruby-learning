#!/opt/chef/embedded/bin/ruby

EmpireStateBuilding = "350 5th Avenue, NYC, NY"
puts EmpireStateBuilding

# Reinitilizing a constant causes ruby to throw warnings.
# Constants are very specific, and don't change over time.
EmpireStateBuilding = "367 7th Street, Oh, Rly?"
puts EmpireStateBuilding
