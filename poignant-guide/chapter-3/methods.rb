
# Method
# methods, are like actions... for example you can open or close a door
# these actions can also be chained together... front_door.open.close
front_door.open

# Method arguments
# methods can also take arguments to use when performing an action
# chained methods can also have chained arguments front_door.paint( 3, :red ).dry( 30 ).close() 
# methods with no arguments can have empty () but they are usually removed, notice .close() 
front_door.paint( 3, :red )

