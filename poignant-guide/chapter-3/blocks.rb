
# Blocks
# any code surrounded by curly braces is a block.
2.times {
 puts 'some example string'
}

# blocks can also use do and end in place of { }
2.times do
 puts 'some example string'
end

# block arguments
# x is the argument, using a whitespace array %w{} we loop through each entry and pass each 
# value as an argument to x
%w{first second third}.each do |x|
  puts x
end
