
# Ranges
# A range is two values surrounded by parentheses and separated by an ellipsis
# (in the form of two or three dots).
(1..3) or ('a'..'z')

# Normally, only two dots are used. If a third dot is used, the last value in the range is excluded.
(1...4).each do |num|
  puts num  
end
